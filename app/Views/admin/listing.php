<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listing</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
 
  <link rel="stylesheet" href="/css/admin.css">

</head>
<body>
    
 <nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
     <img src="/img/dribbble_-_bunny_4x.webp" width="90" height="100" class="d-inline-block align-top" alt="">
     Digitell_Fame.my
    </a>
  </nav>

  <div class="container mt-5">

    <?php if (isset($_SESSION['success'])) :?>
      <div class="row">
        <div class="col">
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong> New data has been added
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
             
          </div>
        </div>
      </div>
   <?php endif; ?>

    <div class="row">
      <div class="col-12">
       <h3>Package Listing</h3>
       <a href = "/gambar/add"   class="btn btn-sm btn-primary float-right">Add New</a>
      </div>
            
      <div class="col-12">

        <table class="table table-striped table-sm">
          <thead class="table-dark ">
             <tr>
                <th>IB</th>
                <th>GAMBAR</th>
                <th>NAMA</th> 
                <th></th>
             </tr>
          </thead>
          <tbody>

           <?php  foreach ($gambar as $g) :?>
             <tr>
               <td> <?= $g ['id'];  ?></td>
               <td>
                 <img class = "gambar-kucing" src="/img/<?= $g ['nama_file']?>"  alt=""> 
               </td>
               <td>
                  <?= $g ['nama']?>
               </td>
               <td>
                  <a href= "/gambar/edit/<?= $g ['id'];?>" class="btn btn-sm btn-primary">EDIT</a>
                  <a href= "/gambar/delete/<?= $g ['id'];?>" class="btn btn-sm btn-danger">DELETE</a>
               </td>
             </tr>
            <?php  endforeach; ?>                
          </tbody>
        </table>
        
        <div id="my-pagination">
          <?= $pager->links() ?>  
        </div>  

          <!--pagination

          <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
              <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>
      </div>
    </div> end of pagination -->
  </div>
        <footer class="text-center p-5">
          <h4><p>ALL RIGHT RESERVED &copy; 2023</p></h4>
        </footer>
</body>
</html>