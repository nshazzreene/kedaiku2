<?php
  
  namespace App\Controllers;

  class Gambar extends BaseController
  {

   private $session; // Declare the session variable

    function __construct()
    {
       $this->session= \Config\Services::session();
    }

    public function index()
     {
       
        $gambar_model = new \App\Models\GambarModel();

        $data = 
          [
              'gambar' => $gambar_model->orderBy('id')->paginate(3),
              'pager' => $gambar_model->pager,
          ];
    
        return view ('admin/listing' ,$data );
      }

    function edit($id)
     {
         helper('form');
         $gambar_model = new \App\Models\GambarModel();
         $gambar = $gambar_model->find($id);

         return view ('admin/edit', ['gambar' => $gambar]);
     }

     public function save_edit($id) // Use 'public' instead of 'function'
     {
         $gambar_model = new \App\Models\GambarModel();
 
         $data = [
             'nama' => $this->request->getPost('nama'),
             'description' => $this->request->getPost('description'),
         ];
 
         $files = $this->request->getFiles('nama_file');
 
         if ($files) {
             $file = $files['nama_file']; // Use array access to get the file
 
             // Generate new secure name
             $nama_file = $file->getRandomName();
 
             // Move file to its new home
             $file->move('img/', $nama_file);
 
             $data['nama_file'] = $nama_file;
         }
         $gambar_model->update($id, $data);
         return redirect()->to('/gambar/edit/'.$id);
     }
 
         public function delete ($id)
         {
          $gambar_model = new \App\Models\GambarModel();
          
          // cari based on id then delete
          $gambar_model->where ('id',$id)->delete();

          return redirect()->back();

         }
      

          function add()
         {
            helper ('form');
            return view ('admin/add');
         }

         public function save_new()
         {
             $gambar_model = new \App\Models\GambarModel();
     
             $data = [
                 'nama' => $this->request->getPost('nama'),
                 'description' => $this->request->getPost('description'),
             ];
     
             $files = $this->request->getFiles('nama_file');
     
             if ($files) {
                 $file = $files['nama_file'];
     
                 // Generate new secure name
                 $nama_file = $file->getRandomName();
     
                 // Move file to its new home
                 $file->move('img/', $nama_file);
     
                 $data['nama_file'] = $nama_file;
             }
     
             $gambar_model->insert($data);
     
             $_SESSION['success'] = true;
             $this->session->markAsFlashdata('success');
     
             return redirect()->to('/gambar');


            
         }
     

}