
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Gambar</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
 
    <link rel="stylesheet" href="css/admin.css">

</head>
<body>
    
  <nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
      <img src="/img/dribbble_-_bunny_4x.webp" width="90" height="100" class="d-inline-block align-top" alt="">
      Digitell_Fame.my
    </a>
  </nav>

  <div class="container mt-5">

  <?php if (isset($_SESSION['success'])) :?>
      <div class="row">
        <div class="col">
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong> New data has been updated
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              
          </div>
        </div>
      </div>
   <?php endif; ?>
   
    <div class="row">
      <div class="col-12 ">
        <h3> <a href="/gambar" class="btn btn-sm btn-primary">Back</a>Edit Gambar</h3>
         <hr>
          <?php echo form_open_multipart('/gambar/edit/' . $gambar ['id']) ?> 
            <div class="form-group row">
              <label for=" nama" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="nama" name="nama" value="<?= $gambar['nama']?>">
                </div>
            </div>

          <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description</label>
             <div class="col-sm-10">
               <textarea class= "form-control" id="description" name= "description" rows="3"><?= $gambar['description']?></textarea>                
              </div>
          </div> 

              <div class="form-group row">
               <label for="description" class="col-sm-2 col-form-label">Image</label>

                <div class="col-sm-10">               
                 <img src="/img/<?= $gambar ['nama_file']; ?>" alt="" style="max-width:300px;"  class="img-fluid">

                 <div class="mb-3">
                  <label for="formFile" class="form-label">Default file input example</label>
                  <input class="form-control" type="file" id="formFile">
                 </div>
                </div>
                <button class="btn btn-primary" type="submit">Save</button>
               </div>
  </div> 

        </form>
     </div>
        </div>
          </div>

      <footer class="text-center p-5">
        <h4><p>ALL RIGHT RESERVED &copy; 2023</p></h4>
      </footer>
</body>
</html>