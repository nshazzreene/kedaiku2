<?php
  
  namespace App\Controllers;

  class Produk extends BaseController
  {

   private $session; // Declare the session variable

    function __construct()
    {
       $this->session= \Config\Services::session();
       $this->produk_model = new \App\Models\ProdukModel();
    }

    public function index()
     {
         $data = 
          [
              'produk' => $this->produk_model->orderBy('id','desc')->paginate(5),
              'pager' => $this->produk_model->pager,
          ];
    
        return view('admin_produk/listing',$data);
      }

    function edit($id)
     {
        $produk_model = new \App\Models\ProdukModel();

       // helper('form');
         $produk = $produk_model->find($id);

         return view('admin_produk/edit', ['produk' => $produk]);
     }

     public function save_edit($id) // Use 'public' instead of 'function'
     {

        $produk_model = new \App\Models\ProdukModel();
         $data = [
             'nama' => $this->request->getPost('nama'),
             'description' => $this->request->getPost('description'),
             'harga' => $this->request->getPost('harga')
         ];
 
         $files = $this->request->getFiles('gambar');
 
         if ($files) {
              $file = $files['gambar']; // Use array access to get the file
 
             // Generate new secure name
             $file_gambar = $file->getRandomName();
 
             // Move file to its new home
             $file->move('img/produk/', $file_gambar);
 
             $data['gambar'] = $file_gambar;
         }
         $produk_model->update($id, $data);

         return redirect()->to('/gambar/edit/'.$id);
     }
 
         public function delete ($id)
         {                  
          $produk_model = new \App\Models\ProdukModel();
        
          // cari based on id then delete
          $produk_model->where('id',$id)->delete();

          $_SESSION['deleted']=true;
          $this->session->markAsFlashdata('deleted');

          return redirect()->back();

         }


          function add()
         {     
            $produk_model = new \App\Models\ProdukModel();   
            return view ('admin_produk/add');
         }

         public function save_new()
         {
             $data = [
                 'nama' => $this->request->getPost('nama'),
                 'description' => $this->request->getPost('description'),
             ];
     
             $files = $this->request->getFiles('nama_file');
     
             if ($files) {
                 $file = $files['nama_file'];
     
                 // Generate new secure name
                 $nama_file = $file->getRandomName();
     
                 // Move file to its new home
                 $file->move('img/', $nama_file);
     
                 $data['nama_file'] = $nama_file;
             }
     
             $this->produk_model->insert($data);
     
             $_SESSION['success'] = true;
             $this->session->markAsFlashdata('success');
     
             return redirect()->to('/produk');


            
         }
     

}